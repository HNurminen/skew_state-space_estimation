Implementations of inference algorithms for state-space models with skew-t-distributed measurement noise
================

## Authors

Henri Nurminen <henri.nurminen@tut.fi> (joint work with Tohid Ardeshiri, Robert Piche, and Fredrik Gustafsson)

## Overview

This repository contains MATLAB codes that implement the algorithms compared in the papers

* H. Nurminen, T. Ardeshiri, R. Piche, and F. Gustafsson. "Robust Inference for State-Space Models with Skewed Measurement Noise". IEEE Signal Processing Letters, vol. 22, no. 11, pp. 1898-1902, November 2015.

* H. Nurminen, T. Ardeshiri, R. Piche, and F. Gustafsson. "Skew-t filter and smoother with improved covariance matrix approximation". 2016. [Online]. Available: http://arxiv.org/abs/1608.07435.

## Instructions for Use

Download the codes using the button Downloads on the left, and go to the downloaded folder in MATLAB. The MATLAB command 
```
#!matlab
   run_skewt_examples
```
generates data from the state-space model with skew-t-distributed measurement errors, applies the algorithms Skew t filter (STF), Skew t variational Bayes filter (STVBF), t variational Bayes filter (TVBF), Kalman filter with gating (KF), Skew t smoother (STS), Skew t variational Bayes smoother (STVBS), t variational Bayes smoother (TVBS), and Rauch-Tung-Striebel smoother with gating (RTSS) to the generated data, and prints the RMS errors.