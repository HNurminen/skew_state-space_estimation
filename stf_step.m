function [x, P] = stf_step(x, P, y, A, Q, C, R, Delta, Nu, Nvb)
% [x, P] = stf_step(x, P, y, A, Q, C, R, Delta, Nu, Nvb)
%
% Computes one measurement update of the Skew t filter.
%
% Input:
%  x - prior mean of the state
%  P - prior covariance matrix of the state
%  y - measurement vector
%  A - state transition matrix
%  Q - process noise covariance matrix
%  C - measurement model matrix
%  R - matrix of spread parameters
%  Delta - matrix of skewness parameters
%  Nu - vector of degrees of freedom
%  Nvb - number of VB iterations (5 by default)
% Output:
%  x - the mean of the posterior
%  P - the covariance matrix of the posterior
%
% Henri Nurminen 30.3.2016

if nargin<10 || isempty(Nvb)
    Nvb = 5;
end

%%% Prediction step

xm = A*x;
Pm = A*P*A' + Q;

%%% Update step

% Missing measurements
m = ~isnan(y);
y = y(m);
C = C(m,:);
Delta = Delta(m,m,:);
R = R(m,m,:);

n_x = numel(x);
n_y = numel(y);
n = n_x+n_y;

xii = 1:n_x;         % indices of the state in (x,u)-vector
uii = n_x+1:n_x+n_y; % indices of u in (x,u)-vector
CD = [C, Delta];     % measurement model matrix for (x,u)
iR = eye(n_y)/R;     % inverse of R

% VB initialization
iLambda = eye(n_y);

for i = 1:Nvb
    
    % VB update for (x,u)
    
    xu0 = [xm; zeros(n_y,1)];
    Pxu0 = [Pm, zeros(n_x,n_y); zeros(n_y,n_x), iLambda];
    Sxu = C*Pm*C' + Delta*iLambda*Delta' + iLambda*R;
    Kxu = [Pm*C'; iLambda*Delta'] / Sxu;
    xu = xu0 + Kxu*(y-C*xm);
    D = eye(n) - Kxu*CD;
    Pxu = D*Pxu0*D' + Kxu*iLambda*R*Kxu';
    
    % Optimal Recursive truncation of (x,u) (Perala & Ali-Loytty 2008)
    undone = uii;
    for kk = 1:numel(uii)
        % Choosing the truncation applied the next.
        stds = sqrt(diag(Pxu));
        [~,ku] = min(xu(undone)./stds(undone));
        k = undone(ku);
        undone(ku) = [];
        
        % Mean and covariance of N(xu,Pxu) truncated by xu(k)>0
        trl = xu(k) / stds(k);
        tralpha = normcdf(trl);
        is_ok = tralpha>1e-100;
        if is_ok
            trepsilon = exp(-0.5*trl^2) / (sqrt(2*pi)*tralpha);
            xu = xu + trepsilon/sqrt(Pxu(k,k)) * Pxu(:,k);
            Pxu = Pxu + (-trl*trepsilon-trepsilon^2)/Pxu(k,k) ...
                * (Pxu(:,k)*Pxu(k,:));
        else
            xu = xu - xu(k)/Pxu(k,k) * Pxu(:,k);
            Pxu = Pxu - 1/Pxu(k,k) * (Pxu(:,k)*Pxu(k,:));
        end
        % Now we approximate the truncated distribution with N(xu,Pxu).
    end
    % Now the approximative VB posterior of (x,u) is N(xu,Pxu).
    
    % VB update for Lambda
    
    dd = y-C*xu(xii)-Delta*xu(uii);
    Psi = (dd*dd'+CD*Pxu*CD')*iR + Pxu(uii,uii) + xu(uii)*xu(uii)';
    iLambda = diag((Nu(m)+diag(Psi))./(Nu(m)+2));
end

x = xu(xii);
P = Pxu(xii,xii);
P = 0.5*(P+P');