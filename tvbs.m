function [xs, Ps] = tvbs(x0, P0, y, A, Q, C, R, Nu, Nvb)
% [xs, Ps] = tvbs(x0, P0, y, A, Q, C, R, Nu, Nvb)
%
% Computes the t variational bayes smoother. The input is one
% measurement set of K time instants. The smoother is a fixed-interval
% smoother.
%
% Input:
%  x - prior mean of the initial state
%  P - prior covariance matrix of the initial state
%  y - 1xK cell array of measurement vectors
%  A - state transition matrix
%  Q - process noise covariance matrix
%  C - 1xK cell array of measurement model matrices
%  R - 1xK cell array of measurement noise shape parameter matrices
%  Nu - 1xK cell array of vectors of degrees of freedom
%  Nvb - number of VB iterations (30 by default)
% Output:
%  xs - n_x x K+1 matrix of the means of the smoother posterior
%  Ps - n_x x n_x x K+1 matrix of the covariances of the smoother posterior
%
% Henri Nurminen 1.3.2015

if nargin<9 || isempty(Nvb)
    Nvb = 30;
end

% Number of time instants
nm = numel(y);

bias = cell(1,nm); % variable for Delta*E_q[u]
ilambdamean = cell(1,nm); % variable for the vector of 1./E[\Lambda_ii]
for i = 1:nm
    % Initializing the VB iteration
    bias{i} = zeros(size(y{i}));
    ilambdamean{i} = ones(size(y{i}));
end

% Variational Bayes iterations
for n = 1:Nvb
    % State variable x
    [xs, Ps] = rtss(x0, P0, y, A, Q, C, R, bias, ilambdamean);
    
    for i = 1:nm
        % Kurtosis variable lambda
        dd = y{i}-C{i}*xs(:,i+1);
        Psi = R{i} \ (dd*dd'+C{i}*Ps(:,:,i+1)*C{i}');
        alpha = Nu{i} + 2;
        beta = Nu{i} + diag(Psi);
        ilambdamean{i} = beta ./ alpha;
    end
end
end


function [xs, Ps] = rtss(x0, P0, y, A, Q, C, R, ybias, ilambdamean)
% [xs, Ps] = rtss(x0, P0, y, A, Q, C, R, ybias, ilambdamean)
%
% Rauch-Tung-Striebel smoother
%
% Henri Nurminen 1.3.2015

nm = numel(y);
n_x = numel(x0);

% Filter predictions:
xmf = nan(n_x,nm);
Pmf = nan(n_x,n_x,nm);
% Filter posteriors:
xf = nan(n_x,nm+1);
Pf = nan(n_x,n_x,nm+1);
% Smoother posteriors:
xs = nan(n_x,nm+1);
Ps = nan(n_x,n_x,nm+1);

% Forward filtering
x = x0;
P = P0;
xf(:,1) = x0;
Pf(:,:,1) = P0;
for i = 1:nm
    xm = A*x;
    Pm = A*P*A' + Q;
    
    CC = C{i};
    S = CC*Pm*CC' + diag(ilambdamean{i}) * R{i};
    K = Pm*CC'/S;
    x = xm + K*(y{i}-ybias{i}-CC*xm);
    P = Pm - K*S*K';
    
    xmf(:,i) = xm;
    Pmf(:,:,i) = Pm;
    xf(:,i+1) = x;
    Pf(:,:,i+1) = P;
end

% Backward smoothing
xs(:,nm+1) = xf(:,nm+1);
Ps(:,:,nm+1) = Pf(:,:,nm+1);
for i = nm-1:-1:0
    G = Pf(:,:,i+1)*A'/Pmf(:,:,i+1);
    
    xs(:,i+1) = xf(:,i+1) + G*(xs(:,i+2)-xmf(:,i+1));
    Ps(:,:,i+1) = Pf(:,:,i+1) + G*(Ps(:,:,i+2)-Pmf(:,:,i+1))*G';
end
end