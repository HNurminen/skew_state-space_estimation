function [x, P] = stvbf_step(x, P, y, A, Q, C, R, Delta, Nu, Nvb)
% [x, P] = stvbf_step(x, P, y, A, Q, C, R, Delta, Nu, Nvb)
%
% Computes one measurement update of the Skew t variational Bayes filter.
%
% Input:
%  x - prior mean of the state
%  P - prior covariance matrix of the state
%  y - measurement vector
%  A - state transition matrix
%  Q - process noise covariance matrix
%  C - measurement model matrix
%  R - diagonal matrix of spread parameters
%  Delta - diagonal matrix of skewness parameters
%  Nu - vector of degrees of freedom
%  Nvb - number of VB iterations (30 by default)
% Output:
%  x - the mean of the posterior
%  P - the covariance matrix of the posterior
%
% Henri Nurminen 1.3.2015

if nargin<10 || isempty(Nvb)
    Nvb = 30;
end

% Prediction step
xm = A*x;
Pm = A*P*A' + Q;

n_y = numel(y);
if n_y==0
    x = xm;
    P = Pm;
    return;
end

% Inverse of the spread parameter matrix
Rinv = eye(size(R))/R;

% Elements that do not chenge over the VB iteration:
tSu = Delta*Delta' + R;
Ku = Delta'/tSu;
% Diagonal elements of the shape matrix of the skewness variable u
% up to the kurtosis factor:
tPu = ones(n_y,1) - diag(Ku*Delta);

% Initializing mean and variance vector of u
umean = zeros(n_y,1);
uvar = ones(n_y,1);

% Intializing the kurtosis variable lambda
mlambda = ones(n_y,1);

% Variational Bayes iterations
for i = 1:Nvb
    Lambdainv = diag(1./mlambda);
    
    % State variable x
    Sx = C*Pm*C' + Lambdainv*R;
    Kx = Pm*C'/Sx;
    x = xm + Kx*(y-Delta*umean-C*xm);
    P = Pm - Kx*Sx*Kx';
    
    % Skewness variable u
    utilde = y-C*x;
    u = Ku * utilde;
    U = Lambdainv * tPu;
    % Mean and variance of the truncated-normal distribution:
    sqrtdiagPu = sqrt(U);
    trl = u ./ sqrtdiagPu;
    tralpha = normcdf(trl);
    is_ok = tralpha>0;
    trepsilon = exp(-0.5*trl(is_ok).^2) ./ (sqrt(2*pi)*tralpha(is_ok));
    umean(is_ok) = u(is_ok) + sqrtdiagPu(is_ok) .* trepsilon;
    uvar(is_ok) = U(is_ok) .* (1-trl(is_ok).*trepsilon-trepsilon.^2);
    % In case of underflow:
    umean(~is_ok) = 0;
    uvar(~is_ok) = 0;
    % The second monents of u
    Y = diag(uvar + umean.^2);
    
    % Kurtosis variable lambda
    Psi = Rinv*(utilde*utilde'+C*P*C') + (Delta*Rinv*Delta+eye(size(Delta)))*Y ...
        - Rinv*Delta*umean*utilde' - Delta*Rinv*utilde*umean';
    alpha = Nu + 2;
    beta = Nu + diag(Psi);
    mlambda = alpha ./ beta;
end

P = 0.5*(P+P');