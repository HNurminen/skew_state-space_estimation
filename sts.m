function [xs, Ps] = sts(x0, P0, y, A, Q, C, R, Delta, Nu, Nvb)
% [xs, Ps] = sts(x0, P0, y, A, Q, C, R, Delta, Nu, Nvb)
%
% Computes the Skew t smoother. The input is one measurement set of K time 
% instants. The smoother is a fixed-interval smoother.
%
% Input:
%  x - prior mean of the initial state
%  P - prior covariance matrix of the initial state
%  y - 1xK cell array of measurement vectors
%  A - state transition matrix
%  Q - process noise covariance matrix
%  C - 1xK cell array of measurement model matrices
%  R - 1xK cell array of measurement noise spread parameter matrices
%  Delta - 1xK cell array of measurement noise skewness parameter matrices
%  Nu - 1xK cell array of vectors of degrees of freedom
%  Nvb - number of VB iterations (5 by default)
% Output:
%  xs - n_x x K+1 matrix of the means of the smoother posterior
%  Ps - n_x x n_x x K+1 matrix of the covariances of the smoother posterior
%
% Henri Nurminen 30.3.2016

if nargin<10 || isempty(Nvb)
    Nvb = 5;
end

nm = numel(y);  % number of time instants
nx = numel(x0); % state dimension
xii = 1:nx;     % indices of the state in (x,u)-vector

% VB initialization of 1./diag(Lambda)
iLambda = cell(1,nm+1);
for ii = 1:nm
    iLambda{ii+1} = ones(numel(y{ii}),1);
end

for i = 1:Nvb
    
    % VB update of (x,u), forward filtering & backward smoothing
    
    % Forward filtering of (x,u) step
    xuf = cell(1,nm+1);
    Pxuf = cell(1,nm+1);
    xuf{1} = x0;
    Pxuf{1} = P0;
    for ii = 1:nm
        xm = A*xuf{ii}(xii);
        Pm = A*Pxuf{ii}(xii,xii)*A' + Q;
        
        % Missing measurements
        m = ~isnan(y{ii});
        yk = y{ii}(m);
        Ck = C{ii}(m,:);
        Deltak = Delta{ii}(m,m);
        Rk = R{ii}(m,m);
        iLambdak = diag(iLambda{ii+1}(m));
        ny = numel(yk);
        n = nx+ny;
        CD = [Ck, Deltak]; % measurement model matrix for (x,u)
        
        xu0 = [xm; zeros(ny,1)];
        Pxu0 = [Pm, zeros(nx,ny); zeros(ny,nx), iLambdak];
        Sxu = Ck*Pm*Ck' + Deltak*iLambdak*Deltak' + iLambdak*Rk;
        Kxu = [Pm*Ck'; iLambdak*Deltak'] / Sxu;
        xu = xu0 + Kxu*(yk-Ck*xm);
        D = eye(n) - Kxu*CD;
        Pxu = D*Pxu0*D' + Kxu*iLambdak*Rk*Kxu';
        
        % Optimal Recursive truncation
        undone = nx+(1:ny);
        for kk = 1:ny
            % Choosing the truncation applied the next.
            stds = sqrt(diag(Pxu));
            [~,ku] = min(xu(undone) ./ stds(undone));
            k = undone(ku);
            undone(ku) = [];
            % Mean and covariance of N(xu,Pxu) truncated by xu(k)>0
            trl = xu(k) / sqrt(Pxu(k,k));
            tralpha = normcdf(trl);
            is_ok = tralpha>1e-100;
            if is_ok
                trepsilon = exp(-0.5*trl^2) / (sqrt(2*pi)*tralpha);
                xu = xu + trepsilon/sqrt(Pxu(k,k)) * Pxu(:,k);
                Pxu = Pxu + (-trl*trepsilon-trepsilon^2)/Pxu(k,k)*(Pxu(:,k)*Pxu(k,:));
            else
                xu = xu - xu(k)/Pxu(k,k) * Pxu(:,k);
                Pxu = Pxu - 1/Pxu(k,k) * (Pxu(:,k)*Pxu(k,:));
            end
        end
        xuf{ii+1} = xu;
        Pxuf{ii+1} = Pxu;
    end
    % Backward smoothing of (x,u) step
    xus = cell(1,nm+1);
    Pxus = cell(1,nm+1);
    xus{nm+1} = xuf{nm+1};
    Pxus{nm+1} = Pxuf{nm+1};
    for ii = nm:-1:1
        ny1 = numel(xuf{ii})-nx;
        ny2 = numel(xuf{ii+1})-nx;
        At = [A, zeros(nx,ny1); zeros(ny2,nx+ny1)];
        Pmi = At*Pxuf{ii}*At' + [Q,zeros(nx,ny2);zeros(ny2,nx),diag(iLambda{ii+1})];
        G = Pxuf{ii}*At'/Pmi;
        
        xus{ii} = xuf{ii} + G*(xus{ii+1}-At*xuf{ii});
        Pxus{ii} = Pxuf{ii} + G*(Pxus{ii+1}-Pmi)*G';
    end
    
    % VB update of Lambda
    
    for ii = 1:nm
        % Missing measurements
        m = ~isnan(y{ii});
        yk = y{ii}(m);
        Ck = C{ii}(m,:);
        Deltak = Delta{ii}(m,m);
        Rk = R{ii}(m,m);
        ny = numel(yk);
        uii = nx+(1:ny);
        CD = [Ck, Deltak]; % measurement model matrix for (x,u)
        
        dd = yk-Ck*xus{ii+1}(xii)-Deltak*xus{ii+1}(uii);
        Psi = (dd*dd'+CD*Pxus{ii+1}*CD')/Rk + Pxus{ii+1}(uii,uii) ...
            + xus{ii+1}(uii)*xus{ii+1}(uii)';
        iLambda{ii+1}(m) = (Nu{ii}(m)+diag(Psi))./(Nu{ii}(m)+2);
    end
end

xs = nan(nx,nm+1);
Ps = nan(nx,nx,nm+1);
for ii = 1:nm+1
    xs(:,ii) = xus{ii}(xii);
    Ps(:,:,ii) = 0.5*(Pxus{ii}(xii,xii)+Pxus{ii}(xii,xii)');
end
end